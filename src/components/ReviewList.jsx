
import db from '../api/db.json';
import { Link } from 'react-router-dom';


const ReviewList = ()=>{
    const reviews = db.reviews;
    
    return(
        <section data-name="review-list">
            <div className="hidden">
                <h4>Categories</h4>
                <p>
                    <select>
                        <option value="">All</option>
                        <option value="bordeaux">Bordeaux</option>
                        <option value="cotes-du-rhone">Côtes du Rhone</option>
                    </select>
                </p>
            </div>
            <grid>
                
                {reviews.map((review, index) => (
                        <div key={index} col="1/2">
                            <card>
                                <a href="/">
                                    <h5>{review.title}</h5>
                                    <p>{review.points} / 100</p>
                                </a>
                            </card>
                            <Link to={`/view/${review.slug}`}>
                                Voir review
                            </Link>
                        </div>
                    ))}
                <div col="1/1" txt="c" className="hidden">
                    <card>
                        <a href="/"><h5>Add a review</h5></a>
                    </card>
                </div>
            </grid>
        </section>

    );
};

export default ReviewList;