import React from 'react'
import Review from './Review'
import ReviewList from './ReviewList'
import NotFound from './NotFound'
import Layout from './Layout'
import {BrowserRouter,Routes,Route} from "react-router-dom";

const App = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Routes>
                    <Route path="/" element={<ReviewList/>}></Route>
                    <Route path="/view/:slug" element={<Review/>}/>
                    <Route path="/view/:random" element={<Review/>}/>

                    <Route path="*" element={<NotFound/>}></Route>
                </Routes>
            </Layout>
        </BrowserRouter>
            
    );    
        
        
    
};

export default App