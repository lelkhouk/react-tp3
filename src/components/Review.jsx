
import { useParams } from 'react-router-dom';
import db from '../api/db.json';

const Review = ()=>{
    
    const {slug} = useParams();
    const review = db.reviews.find((review)=>review.slug===slug);

   
    if(slug === ":random"){
        const randomReviews = db.reviews;
        const randomIndex = Math.floor(Math.random() * randomReviews.length);
        console.log(randomReviews[randomIndex]);

        const randomReview = randomReviews[randomIndex];

        return(
            <section data-name="review">
                <h2>Nouveau</h2>
                <h3>{randomReview.title}</h3>
                <p>
                    Price : <b>{randomReview.price} $</b>
                </p>
                <blockquote>
                    <p>
                        {randomReview.description}
                    </p>
                    <p txt="r">
                        <i>- {randomReview.taster_name}</i>
                    </p>
                </blockquote>
            </section>
        )
    }else if(!review){
        return(
            
            <div> la review n'a pas ete trouvé..</div>
        )
    }else{
        return (
            <section data-name="review">
                <h2>Nouveau</h2>
                <h3>{review.title}</h3>
                <p>
                    Price : <b>{review.price} $</b>
                </p>
                <blockquote>
                    <p>
                        {review.description}
                    </p>
                    <p txt="r">
                        <i>- {review.taster_name}</i>
                    </p>
                </blockquote>
            </section>
            );
    }

};

export default Review;